import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class EditTopic extends Component {
  constructor(props) {
    super(props);

    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      title: '',
      description: '',
      date: new Date(), 
      image: '',
      previewImage: null
    }
  }

  componentDidMount() {
    axios.get('/topics/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          title: response.data.title,
          description: response.data.description,
          date: new Date(response.data.date),
          image: response.data.image
        })   
      })
      .catch(function (error) {
        console.log(error);
      })

    // axios.get('http://localhost:5000/users/')
    //   .then(response => {
    //     if (response.data.length > 0) {
    //       this.setState({
    //         users: response.data.map(user => user.username),
    //       })
    //     }
    //   })
    //   .catch((error) => {
    //     console.log(error);
    //   })

  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    })
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    })
  }


  onChangeDate(date) {
    this.setState({
      date: date
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const topic = {
      title: this.state.title,
      description: this.state.description,
      date: this.state.date,
      image: this.state.image
    }


    axios.post('/topics/update/' + this.props.match.params.id, topic)
      .then(res => console.log(res.data));

    window.location = '/';
  }
  


    // function to upload image once it has been captured
  // includes multer and firebase methods
  uploadImage(e, method) {
    let imageObj = {};

      let imageFormObj = new FormData();

      imageFormObj.append("imageName", "image-" + Date.now());
      imageFormObj.append("imageData", e.target.files[0]);

      // stores a readable instance of 
      // the image being uploaded using multer
      this.setState({
        previewImage: URL.createObjectURL(e.target.files[0])
      });

      axios.post(`/image/upload`, imageFormObj)
        .then((data) => {
          if (data.data.success) {
            alert("Image has been successfully uploaded");
            this.setState({
            image: data.data.document._id
               });
          }
        })
        .catch((err) => {
          alert("Error while uploading image");
          console.log(err);
        });
   
    
  }

  render() {
    let previewImage;

    if (this.state.previewImage != null) {
      previewImage = <img src={this.state.previewImage} alt="upload-image" className="process__image" />
    } else {
      previewImage = <span></span>;
    }

    let src = "/image/get/"+this.state.image;

    return (
    <div>
      <img src={src}/>
      <h3>Edit Topic</h3>
      <form onSubmit={this.onSubmit}>
      <div className="form-group">
          <label>Title: </label>
          <input 
              type="text" 
              className="form-control"
              value={this.state.title}
              onChange={this.onChangeTitle}
              />
        </div>
        <div className="form-group"> 
          <label>Description: </label>
          <textarea  type="text"
              rows="10"
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
              />
        </div>
      
        <div className="form-group">
          <label>Date: </label>
          <div>
            <DatePicker
              selected={this.state.date}
              onChange={this.onChangeDate}
            />
          </div>
        </div>
        <div className="process">
            <h4 className="process__heading">Image Upload</h4>
            <p className="process__details">Upload image to your topic</p>

            <input type="file" className="process__upload-btn" onChange={(e) => this.uploadImage(e, "multer")} />

        {previewImage}
           
          </div><br/>

        <div className="form-group">
          <input type="submit" value="Edit Topic" className="btn btn-primary" />
        </div>
      </form>
    </div>
    )
  }
}