// routes/index.js
const router = require('express').Router();
const topicRoutes = require('./topics');
const path = require('path');

// API routes
router.use('/topics', topicRoutes);


// If no API routes are hit, send the React app
router.use(function(req, res) {
	res.sendFile(path.join(__dirname, '../client/build/index.html'));
});

module.exports = router;