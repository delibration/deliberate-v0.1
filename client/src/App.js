import React from 'react';
// import './App.css';
import { BrowserRouter as Router, Route } from "react-router-dom"
import "bootstrap/dist/css/bootstrap.min.css";

import Navbar from "./components/navbar.component"
import TopicsList from "./components/topics-list.component";
import EditTopic from "./components/edit-topic.component";
import CreateTopic from "./components/create-topic.component";
import ShowTopic from "./components/show-topic.component";

function App() {
	return (
		<Router>
		<div className="container">
			<Navbar />
			<br/>
			<Route path="/" exact component={TopicsList} />
			<Route path="/show/:id" component={ShowTopic} />
			<Route path="/edit/:id" component={EditTopic} />
			<Route path="/create" component={CreateTopic} />
		</div>
		</Router>
	);
}

export default App;
