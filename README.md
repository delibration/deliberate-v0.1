Deliberation Platform using MangoDB, React.js, Express.js & Node.js


steps to install:


`git clone https://github.com/HHS-WP3/deliberate-v0.1.git`

`cd deliberate-v0.1/`

`npm install`

`cd client/ && npm install && cd ..`

`npm run build`

`npm start`


## Structure
backend is in the backendfolder
- models: structure for the database
- routes: definition of the different endpoints
- server.js: main configuration of the server

frontend is in src...

## Endpoints
- topics/
- topics/add
- topics/delete/:id
- topics/update/:id



# Known issues:



