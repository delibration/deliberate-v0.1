var express = require('express');
var Image = require('../models/image.model');
var ImageRouter = express.Router();
const multer = require('multer');
var path = require("path");

const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function (req, file, cb) {
        cb(null, Date.now() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        // rejects storing a file
        cb(null, false);
    }
}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});

/* 
    stores image in uploads folder
    using multer and creates a reference to the 
    file
*/
ImageRouter.route("/upload")
    .post(upload.single('imageData'), (req, res, next) => {
        console.log(req.body);
        const newImage = new Image({
            imageName: req.body.imageName,
            imageData: req.file.path
        });

        newImage.save()
            .then((result) => {
                console.log(result);
                res.status(200).json({
                    success: true,
                    document: result
                });
            })
            .catch((err) => next(err));
    });


ImageRouter.route("/:id").get((req, res) => {
  Image.findById(req.params.id)
    .then(image => res.json(image))
    .catch(err => res.status(400).json('Error: ' + err));
});

ImageRouter.route("/get/:id").get((req, res) => {
  Image.findById(req.params.id)
    .then(image => res.sendFile(path.resolve(image.imageData)))
    .catch(err => res.status(400).json('Error: ' + err));
});



ImageRouter.route('/').get((req, res) => {
  Image.find()
    .then(image => res.json(image))
    .catch(err => res.status(400).json('Error: ' + err));
});

module.exports = ImageRouter;