const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const routes = require('./routes');
const bodyParser = require('body-parser');
const path = require('path')

require('dotenv').config();

const app = express();
const port = process.env.PORT || 5000;

app.use(cors());
app.use(express.json());

// const uri = process.env.ATLAS_URI;
// mongoose.connect(uri, { useNewUrlParser: true, useCreateIndex: true }
// );
// const connection = mongoose.connection;
// connection.once('open', () => {
//   console.log("MongoDB database connection established successfully");
// })

const connection = "mongodb+srv://admin:letmedeliberate2020@cluster0.tv72l.gcp.mongodb.net/<dbname>?retryWrites=true&w=majority";
mongoose.connect(connection,{ useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false})
    .then(() => console.log("Database Connected Successfully"))
    .catch(err => console.log(err));

// const topicsRouter = require('./routes/topics');

// app.use('/topics', topicsRouter);

// configure body parser for AJAX requests
// app.use(express.urlencoded({ extended: true }));
// app.use(express.json());

//app.use(express.static('client/build'));

//app.use(routes);

app.use(bodyParser.json());
app.use(cors());

// API
const topicsRouter = require('./routes/topics');
var imageRouter = require('./routes/image');

app.use('/topics', topicsRouter);
app.use('/uploads', express.static('uploads'));
app.use('/image', imageRouter);

app.use(express.static(path.join(__dirname, './client/build')))
app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, './client/build'))
})



app.listen(port, () => {
    console.log(`Server is running on port: ${port}`);
});