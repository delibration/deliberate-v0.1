import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";

export default class ShowTopic extends Component {
  constructor(props) {
    super(props);

    this.state = {
     topic: {},
     image: null
    }
  }

  componentDidMount() {
    axios.get('/topics/'+this.props.match.params.id)
      .then(response => {
        this.setState( this.setState({ topic: response.data }))
        console.log(this.state) 
      })
      .catch(function (error) {
        console.log(error);
      })

  }

  

  render() {
    let src = "/image/get/"+this.state.topic.image;
  
    return (
    <div>
      <img class="img-thumbnail" src={src}/>
      <h1>{this.state.topic.title}</h1>
      <p>{this.state.topic.description}</p>
      <p  className="text-muted">{this.state.topic.date}</p>
    </div>
    )
  }
}