import React, { Component } from 'react';
import axios from 'axios';
import DatePicker from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";


export default class CreateTopic extends Component {
  constructor(props) {
    super(props);

    this.onChangeTitle = this.onChangeTitle.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeDate = this.onChangeDate.bind(this);
    this.onSubmit = this.onSubmit.bind(this);

    this.state = {
      description: '',
      title: '',
      date: new Date(),
      image: null,
      imageid: null
    }
  }

  onChangeTitle(e) {
    this.setState({
      title: e.target.value
    })
  }

  onChangeDescription(e) {
    this.setState({
      description: e.target.value
    })
  }

  onChangeDate(date) {
    this.setState({
      date: date
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const topic = {
      title: this.state.title,
      description: this.state.description,
      date: this.state.date,
      image: this.state.imageid
    }

    axios.post('/topics/add', topic)
      .then(res => console.log(res.data));

    window.location = '/';
  }

  // function to upload image once it has been captured
  // includes multer and firebase methods
  uploadImage(e, method) {
    let imageObj = {};

      let imageFormObj = new FormData();

      imageFormObj.append("imageName", "image-" + Date.now());
      imageFormObj.append("imageData", e.target.files[0]);

      // stores a readable instance of 
      // the image being uploaded using multer
      this.setState({
        image: URL.createObjectURL(e.target.files[0])
      });

      axios.post(`/image/upload`, imageFormObj)
        .then((data) => {
          if (data.data.success) {
            alert("Image has been successfully uploaded");
            this.setState({
            imageid: data.data.document._id
               });
            console.log(this.state)
          }
        })
        .catch((err) => {
          alert("Error while uploading image");
          console.log(err);
        });
   
    
  }

  render() {
    let image;

    if (this.state.image != null) {
      image = <img src={this.state.image} alt="upload-image" className="process__image" />
    } else {
      image = <span></span>;
    }

    return (
    <div>

      <h3>Create New Topic</h3>
      <form onSubmit={this.onSubmit}>
       <div className="form-group">
          <label>Title </label>
          <input 
              type="text" 
              required
              className="form-control"
              value={this.state.title}
              onChange={this.onChangeTitle}
              />
        </div>
        <div className="form-group"> 
          <label>Description: </label>
          <textarea rows="10" type="text"
              className="form-control"
              value={this.state.description}
              onChange={this.onChangeDescription}
              />
        </div>
       
        <div className="form-group">
          <label>Date: </label>
          <div>
            <DatePicker
              selected={this.state.date}
              onChange={this.onChangeDate}
            />
          </div>
        </div>

        <div className="form-group">
          <input type="submit" value="Create Topic" className="btn btn-primary" />
        </div>
      </form>
     <div className="process">
            <h4 className="process__heading">Image Upload</h4>
            <p className="process__details">Upload image to your topic</p>

            <input type="file" className="process__upload-btn" onChange={(e) => this.uploadImage(e, "multer")} />

        {image}
           
          </div>



 
  </div>
 
    )
  }
}