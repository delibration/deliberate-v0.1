import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import axios from 'axios';

const Topic = props => (
  <div class="card">
   <img class="card-img-top" src={props.image}  />
    <div class="card-body">
     
      <h5 class="card-title"><Link to={"/show/"+props.topic._id}>{props.topic.title}</Link></h5>
      <p class="card-text">{props.topic.description.substring(0,120)}...</p>
      <p class="card-text"><small class="text-muted">{props.topic.date.substring(0,10)}</small></p>
      <p class="card-text"><Link to={"/edit/"+props.topic._id}>edit</Link> | <a href="#" onClick={() => { props.deleteTopic(props.topic._id) }}>delete</a></p>
    </div>
  </div>
)

export default class TopicsList extends Component {
  constructor(props) {
    super(props);

    this.deleteTopic = this.deleteTopic.bind(this)

    this.state = {topics: []};
  }

  componentDidMount() {
    axios.get('/topics/')
      .then(response => {
        this.setState({ topics: response.data })
      })
      .catch((error) => {
        console.log(error);
      })
  }

  deleteTopic(id) {
    axios.delete('/topics/'+id)
      .then(response => { console.log(response.data)});

    this.setState({
      topics: this.state.topics.filter(el => el._id !== id)
    })
  }

  topicList() {
    return this.state.topics.map(currenttopic => {
      let src = "/image/get/"+currenttopic.image;
      return <Topic topic={currenttopic} deleteTopic={this.deleteTopic} image={src} key={currenttopic._id}/>;
    })
  }

  render() {
    return (
      <div>
        <h3>Topics</h3>
        <div class="card-columns">
            { this.topicList() }
         </div>
      </div>
    )
  }
}